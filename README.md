Essentia.js PoC
===============

This is a Proof of Concept of running Essentia in the browser. The strategy
is to use emscripten to compile Essentia + some glue code into JavaScript and
then use it from the browser, using some more glue code.

This PoC is trying to reproducte the work of [Giuseppe Bandiera](https://github.com/giuband)
that at some point he made it work.

```
JS/HTML --- JS glue --- C++ glue --- Essentia (C++)
```

It consists of the following files:

* test.html (JS/HTML)
* clinicAnalyserWorker (JS glue)
* analysis.h (C++ glue)
* analysis.cpp (C++ glue)


Preparing compilation
---------------------

You need to place Essentia library in this directory, typically by cloning the repository:

```
git clone https://github.com/MTG/essentia.git
```

You also need a [working emscripten setup](http://kripken.github.io/emscripten-site/docs/getting_started/downloads.html).


Compiling
---------

```
# configure Essentia

cd essentia
emconfigure sh -c './waf configure --prefix=$EMSCRIPTEN/system/local/ --lightweight= --include-algos=Loudness,Windowing,Spectrum,FFT,Magnitude,PitchYin,PeakDetection,SpectralPeaks,Dissonance --fft=KISS --emscripten'

# compile Essentia

emmake ./waf
emmake ./waf install

# compile C++ glue code

cd .. # back to the repo root

LIB_DIR=$EMSCRIPTEN/system/local/lib

emcc -c analysis.cpp -o analysis.bc -std=c++11 -Oz && emcc analysis.bc ${LIB_DIR}/libessentia.dylib -o clinic_analysis.js -Oz -s EXPORTED_FUNCTIONS="['_init', '_compute', '_shutdown_essentia']"

```

Testing
-------

Serve this directory and open test.html. It will ask for access to the microphone to start processing the sound. Results of the processing should appear in the JS console.


Debugging
---------

In order to get understandable information on C++ Exceptions you should compile the C++ glue code with `-O1` instead of `-Oz`, and add `-s ASSERTIONS=1 -s DISABLE_EXCEPTION_CATCHING=2`
