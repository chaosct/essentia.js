//
//  analysis.cpp
//  essentia_test
//
//  Created by Giuseppe Bandiera on 26/04/16.
//  Copyright © 2016 Giuseppe Bandiera. All rights reserved.
//

#include <essentia/algorithmfactory.h>
#include <essentia/essentiamath.h> // for the isSilent function
#include "analysis.h"

using namespace essentia;
using namespace standard;

struct AlgStruct {
    Algorithm * loudness;
    Algorithm * equalLoudness;
    Algorithm * window;
    Algorithm * spectrum;
    Algorithm * pitchYinFFT;
    Algorithm * spectralPeaks;
    Algorithm * dissonance;
};

extern "C" {
    
    AlgStruct * init(int framesize, int sr) {
        /**
         Initialize essentia algorithms and store them in a data structure.
         Return the pointer to this data structure so that we can use the algorithms later.
         */
        essentia::init();
        AlgorithmFactory& factory = AlgorithmFactory::instance();
        AlgStruct * alg_struct = new AlgStruct;
        alg_struct->loudness = factory.create("Loudness");
        
        // return alg_struct;

        alg_struct->equalLoudness = factory.create("EqualLoudness",
                                                   "sampleRate", sr);

        alg_struct->window = factory.create("Windowing",
                                            "type", "hann",
                                            "zeroPadding", 0);

        
        alg_struct->spectrum = factory.create("Spectrum",
                                              "size", framesize);
        
        alg_struct->pitchYinFFT = factory.create("PitchYinFFT",
                                                 "frameSize", framesize,
                                                 "sampleRate", sr);
        
        alg_struct->spectralPeaks = factory.create("SpectralPeaks");
         
        alg_struct->dissonance = factory.create("Dissonance");

        return alg_struct;
    }

    
    void compute(AlgStruct * alg_struct, float * audio_track, int numSamples, float * analysis_result) {
        
        // declare variables where we'll store results
        std::vector<Real> equalized_signal;
        std::vector<Real> windowed_frame;
        std::vector<Real> spec;
        std::vector<Real> spec_peak_freq;
        std::vector<Real> spec_peak_mag;
        Real loud;
        Real diss;
        Real pitch;
        Real pitch_conf;
        
        // convert audio_track's array of float to a vector
        std::vector<float> audio_frame(audio_track, audio_track + numSamples);
        
        if (isSilent(audio_frame))
            return;
        
        // get pointer to initialized algorithms
        Algorithm* loudness = alg_struct->loudness;
        Algorithm* equalLoudness = alg_struct->equalLoudness;
        Algorithm* window = alg_struct->window;
        Algorithm* spectrum = alg_struct->spectrum;
        Algorithm* pitchYinFFT = alg_struct->pitchYinFFT;
        Algorithm* spectralPeaks = alg_struct->spectralPeaks;
        Algorithm* dissonance = alg_struct->dissonance;
        
        
        // CONFIGURATION
        
        // configure equal loudness
        equalLoudness->input("signal").set(audio_frame);
        equalLoudness->output("signal").set(equalized_signal);
        
        // configure loudness
        loudness->input("signal").set(equalized_signal);
        loudness->output("loudness").set(loud);
        
        // configure windowing:
        window->input("frame").set(equalized_signal);
        window->output("frame").set(windowed_frame);
        
        // configure spectrum:
        spectrum->input("frame").set(windowed_frame);
        spectrum->output("spectrum").set(spec);
        
        // configure pitch detector:
        pitchYinFFT->input("spectrum").set(spec);
        pitchYinFFT->output("pitch").set(pitch);
        pitchYinFFT->output("pitchConfidence").set(pitch_conf);
        
        // configure spectral peaks:
        spectralPeaks->input("spectrum").set(spec);
        spectralPeaks->output("frequencies").set(spec_peak_freq);
        spectralPeaks->output("magnitudes").set(spec_peak_mag);
        
        // configure dissonance:
        dissonance->input("frequencies").set(spec_peak_freq);
        dissonance->input("magnitudes").set(spec_peak_mag);
        dissonance->output("dissonance").set(diss);
        
        
        // ANALYSIS
        
        // preprocess: compute equalized signal
        equalLoudness->compute();
        
        // compute loudness
        loudness->compute();
        
        // compute pitch
        window->compute();
        spectrum->compute();
        pitchYinFFT->compute();
        
        // compute dissonance
        spectralPeaks->compute();
        dissonance->compute();
        
        analysis_result[0] = loud;
        analysis_result[1] = pitch;
        analysis_result[2] = pitch_conf;
        analysis_result[3] = diss;
    }
    
    void shutdown_essentia() {
        essentia::shutdown();
    }
}