//
//  analysis.hpp
//  essentia_test
//
//  Created by Giuseppe Bandiera on 26/04/16.
//  Copyright © 2016 Giuseppe Bandiera. All rights reserved.
//

#ifndef analysis_h
#define analysis_h

struct AlgStruct;

extern "C" {
    AlgStruct * init(int framesize, int sr);
    void compute(AlgStruct * alg_struct, float * audio_track, int numSamples, float * analysis_result);
    void shutdown_essentia();
}

#endif /* analysis_h */
