// vars for storing audio buffer in memory efficiently
var audioFrame;
var frameSize;
var sampleRate;
var hopSize;
var algStructPointer;
var pushIndex;
var resultPtr;

// global vars used for incremental uploading
var bufferIndex = 0;
var customPerformanceKey;
var sessionID = 0;

function init(config) {
  /**
   * Initializes the essentia algorithms for the analysis.
   * @type {number}
   */
  frameSize = config.frameSize;
  sampleRate = config.sampleRate;
  hopSize = config.hopSize;
  // define Module.onRuntimeInitialized() in order to avoid 'wait for main() to be called' error

  // Module = {
  //   onRuntimeInitialized: function() {
  //     algStructPointer = Module._init(frameSize, sampleRate);
  //     pushIndex = frameSize - hopSize;
  //     // create space in memory where to store rt analysis results (loudness, pitch, etc)
  //     var result = new Float32Array(4);
  //     var result_data_bytes = result.length * result.BYTES_PER_ELEMENT;
  //     resultPtr = Module._malloc(result_data_bytes);
  //     postMessage('init OK');
  //   }
  // };
  // IMPORTANT: leave this line AFTER the definition of Module
  importScripts('clinic_analysis.js');
        algStructPointer = Module._init(frameSize, sampleRate);
      pushIndex = frameSize - hopSize;
      // create space in memory where to store rt analysis results (loudness, pitch, etc)
      var result = new Float32Array(4);
      var result_data_bytes = result.length * result.BYTES_PER_ELEMENT;
      resultPtr = Module._malloc(result_data_bytes);
      postMessage('init OK');
}


function initSession(baseSessionID) {
  /**
   * Initializes variables needed for identifying the current session.
   * @type {number}
   */
  bufferIndex = 0;
  audioFrame = new Float32Array(frameSize);
  customPerformanceKey = sessionID + '_' + baseSessionID;
  sessionID++;
  postMessage({ performanceKey: customPerformanceKey });
}

function destroy() {
  Module._shutdown_essentia();
}

function analysis(inputBuffer) {
  /*
      FRAME GENERATION
      We need to manually implement a frame cutter.
      The idea is to have a frame (audioFrame) whose length is frameSize;
      we constantly replace its last #hopsize elements with the incoming
      elements after having shifted its content by #hopsize elements to the left.
   */

    // remove the first #hopSize elements in order to shift the content of audioFrame
  var audioFrame_tmp = audioFrame.subarray(hopSize);
  audioFrame.set(audioFrame_tmp);
  // concatenate with the incoming data, that will be put at the end (at the index pushIndex)
  audioFrame.set(new Float32Array(inputBuffer), pushIndex);

  bufferIndex++;

  /*
      ANALYSIS
   */
  if (bufferIndex >= (frameSize / hopSize)) {  // discard initial frames
    // store audioFrame in the heap so that the c++ code can access it
    var nDataBytes = audioFrame.length * audioFrame.BYTES_PER_ELEMENT;
    var dataPtr = Module._malloc(nDataBytes);
    Module.HEAPF32.set(audioFrame, dataPtr >> 2);

    // call analysis method
    Module._compute(algStructPointer, dataPtr, audioFrame.length, resultPtr);

    // access analysis results
    var loudness = Module.HEAPF32[(resultPtr >> 2)];
    var pitch = Module.HEAPF32[(resultPtr >> 2) + 1];
    var pitchConf = Module.HEAPF32[(resultPtr >> 2) + 2];
    var dissonance = Module.HEAPF32[(resultPtr >> 2) + 3];
    // free memory
    Module._free(dataPtr);
  }

  postMessage({
    audioData: inputBuffer,
    loudness: loudness,
    pitch: pitch,
    pitchConf: pitchConf,
    dissonance: dissonance,
  });
}

this.onmessage = function onmessage(e) {
  switch (e.data.command) {
    case 'init':
      init(e.data.config);
      initSession("hola");
      break;
    case 'analysis':
      analysis(e.data.buffer);
      break;
    case 'initSession':
      break;
    case 'destroy':
      destroy();
      break;
    default:
      break;
  }
};
